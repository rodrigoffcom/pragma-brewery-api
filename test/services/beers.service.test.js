const BeersService = require('../src/services/beers.service');

describe('BeersService', () => {
  describe('find', () => {
    it('should find all the beers available', () => {
      const sut = new BeersService();

      const result = sut.find();

      expect(result).not.toBeNull();
      expect(result.length).toBeGreaterThan(0);
    });
  });
});
