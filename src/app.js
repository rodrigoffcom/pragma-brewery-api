const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;

async function seedBeers() {
  await app.service('beers').create({
    id: 1,
    name: 'Acid Dillinger',
    category: 'Pilsner',
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
    adequateTemperature: { low: -6, high: -4 },
  });

  await app.service('beers').create({
    id: 2,
    name: 'Humble & Life',
    category: 'IPA',
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
    adequateTemperature: { low: -6, high: -5 },
  });

  await app.service('beers').create({
    id: 3,
    name: 'Cowboy Dropsy',
    category: 'Lager',
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
    adequateTemperature: { low: -7, high: -4 },
  });

  await app.service('beers').create({
    id: 4,
    name: 'Hysterical & Lizard',
    category: 'Stout',
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
    adequateTemperature: { low: -8, high: -6 },
  });

  await app.service('beers').create({
    id: 5,
    name: 'Hootenanny Mystery Box',
    category: 'Wheat Beer',
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
    adequateTemperature: { low: -5, high: -3 },
  });

  await app.service('beers').create({
    id: 6,
    name: 'Little Boots',
    category: 'Pale Ale',
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
    adequateTemperature: { low: -6, high: -4 },
  });
}

async function updateBeerTemperature() {
  const id = Math.random() * (1, 6) + 1;
  await app.service('beers').patch(id, {
    temperature: (Math.random() * (1, 10) + 1).toFixed(1) * -1,
  });
}

seedBeers();
setInterval(updateBeerTemperature, (Math.random() * (10, 60) + 5) * 1000);
