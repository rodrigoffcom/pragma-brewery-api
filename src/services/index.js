const BeersService = require('./beers.service');

module.exports = function(app) {
  app.use('/beers', new BeersService());
};
