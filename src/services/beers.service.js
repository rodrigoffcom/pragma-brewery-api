class BeersService {
  constructor() {
    this.data = [];
  }

  async find() {
    return this.data;
  }

  async get(id) {
    const beer = this.data.find(d => d.id === parseInt(id, 10));

    if(!beer) {
      throw new Error(`Beer with id ${id} not found`);
    }

    return beer;
  }

  async create(data) {
    this.data.push(data);
    return data;
  }

  async patch(id, data) {
    const message = await this.get(id);
    return Object.assign(message, data);
  }
}

module.exports = BeersService;
